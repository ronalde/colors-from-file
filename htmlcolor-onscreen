#!/usr/bin/env bash

## script to print colored text according to input which should be a
## HTML (hex) color code (like #fff).
## 
##  Copyright (C) 2019 Ronald van Engelen <ronalde+gitlab@lacocina.nl>
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.
##
## Source:    https://gitlab.com/ronalde/colors-from-file

hex_color="$1"
scriptname="htmlcolor-onscreen"
msg_usage="
USAGE:
 ${scriptname} \"#f00\"
-or- 
 ${scriptname} \"#ff0000\"

 NOTE: Terminal should support truecolor.
"

ansi_init="\e["
ansi_reset="\033[0m"
ansi_format=2
ansi_bg=48
ansi_fg=38

# The ;2 and ;5 indicate the format of the color, ultimately telling
# the terminal how many more sequences to pull:
# ;5 specifies an 8-bit format requiring only 1 more control segment
# ;2 specifies a full 24-bit RGB format requiring 3 segments
# control segments.

function display_usage() {
    if [[ "${SKIPUSAGE}x" == "x" ]]; then
	printf "%s\n" "${msg_usage}"
    fi
}

function hex_2_dec() {
    printf "%d" "0x$1" 2>/dev/null
}

function dec_2_hex() {
    printf "%x" "$1" 2>/dev/null
}

function ansi_esc() {
    fg_bg="$1"
    rgb_ansi="$2"
    declare -p fg_bg rgb_ansi 1>&2
    printf '\033[%s;5;%sm' \
	   "${fg_bg}" \
	   "${rgb_ansi}" 1>&2
    printf '\%b%s;5;%sm' \
	   '033[' \
	   "${fg_bg}" \
	   "${rgb_ansi}"
}


function color_on_screen() {
    hex_color="$1"
    oct_re="[0-9a-zA-Z]{1,2}"
    hex_re="#(${oct_re})(${oct_re})(${oct_re})"
    if (( ${#hex_color} == 4 )) || (( ${#hex_color} == 7 )); then
	if [[ "${hex_color}" =~ ${hex_re} ]]; then
	    hex_r=${BASH_REMATCH[1],,}
	    hex_g=${BASH_REMATCH[2],,}
	    hex_b=${BASH_REMATCH[3],,}
	    if (( ${#hex_r} == 1 )); then
		## #0fe
		hex_r="${hex_r}${hex_r}"
		hex_g="${hex_g}${hex_g}"
		hex_b="${hex_b}${hex_b}"
	    fi
	    ## convert hex octects to decimal values
	    printf -v dec_r "%d" "0x${hex_r}"
	    printf -v dec_g "%d" "0x${hex_g}"
	    printf -v dec_b "%d" "0x${hex_b}"
	    h_rgb="${dec_r};${dec_g};${dec_b}"
	    c_ansi_fg="${ansi_init}${ansi_fg};${ansi_format};${h_rgb}m"
	    c_ansi_bg="${ansi_init}${ansi_bg};${ansi_format};${h_rgb}m"
	    
	    printf 1>&2 "%b rgb(" \
			"${c_ansi_fg}" 
	    printf "%3d,%3d,%3d" \
		   "${dec_r}" \
		   "${dec_g}" \
		   "${dec_b}" 
	    printf 1>&2 ")%b " \
			"${ansi_reset}"
	    
	    printf  1>&2 "%b%-7s%b\n" \
			 "${c_ansi_bg}" \
			 "${hex_color}" \
			 "${ansi_reset}"
	else
	    return 1
	fi
    else
	return 1
    fi
    
}

scriptdir="${scriptdir:-$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd )}"

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    SKIPUSAGE="${SKIPUSAGE:-}"
    if [[ "${hex_color}x" == "x" ]]; then
	display_usage
	exit 1
    fi
    
    if ! color_on_screen "${hex_color}"; then
	printf 1>&2 "error: input (\`%s') is not an html hex color (like '#fff' or '#FFFFFF')\n"  \
		    "${hex_color}"
	display_usage
	exit 1
    fi
else
    debug "\`${BASH_SOURCE[0]}' sourced from \`${scriptdir}/${0##/*}'."
fi
