# colors-from-file

Simple bash script which visualizes hex color definitions in text
files both on a terminal screen and in a html file.

## Usage

./colors-from-file PATH

Where PATH is the path to a text file.

## Sample output

### On terminal

[![asciicast](https://asciinema.org/a/225422.svg)](https://asciinema.org/a/225422)

### In temporary html file

![colors-from-file.tomorrow-night-80s.tsulhv.html](/uploads/46bdbca7ff8ae10adc519f07da64d884/colors-from-file.tomorrow-night-80s.tsulhv.html.jpg)


## Installation

```bash
git clone https://gitlab.com/ronalde/colors-from-file.git
```

## Rationale

Provide easy and fast visualization of colors defined in css files and
configuration files like `~/.Xresources` and `~/.config/i3/config`.

